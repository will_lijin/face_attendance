# 模块划分

1. 视频采集 -- V4L，OpenCV
2. 人脸检测 -- 离线检测，识别照片中的哪一部分是人脸
3. 人脸识别 -- 在线识别，对比采集的照片中和事先录入的照片是否是同一个人
4. 入库 -- 记录到考勤机本地的数据库中
5. 信息显示 -- 直接显示到屏幕上

硬件：摄像头（分辨率），ARM Cortex A系列开发板，本地存储（Flash容量），TFT液晶屏幕（分辨率，大小），内存容量，网络连接方式（可选，有线/无线，带宽）

raspberry pi 树莓派

软件：Linux系统（Raspbian/debian10），OpenCV（3.2版本），云服务



## 什么是OpenCV

OpenCV Intel公司发布的一款计算机视觉库，用于图像分析。

## 用OpenCV做什么

调用摄像头驱动采集图像，对图像进行预处理，用于人脸检测的物体检测算法（基于决策树的机器学习算法）

## 安装Raspbian

1. （可选）修改软件源 https://mirrors.tuna.tsinghua.edu.cn/help/debian/
2. 安装vim

```bash
sudo apt install vim
```

3. 安装opencv开发软件包

```bash
sudo apt update
sudo apt install libopencv-dev
```

## 打开SSH服务

```bash
sudo raspi-config
```



## OpenCV读取摄像头

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

int main()
{
    VideoCapture cam(0); //创建摄像头对象
    namedWindow("Camera"); //创建名字为Camera的窗口
    
    Mat image;
    
    while(1)
    {
        cam >> image; //从摄像头读取一帧图像
    	imshow("Camera", image); //在Camera窗口显示摄像头捕获到的图像
    	if (waitKey(10) != 255) //延时10毫秒，判断是否有按键按下，如果按下结束程序
        {
            break;
        }
    }
}
```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui 
```



## OpenCV人脸检测

```C++
#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

int main()
{
	VideoCapture cam(0);
    //创建级联分类器，并加载人脸检测模型文件
	CascadeClassifier classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");

	namedWindow("Camera");

	Mat image;
	Mat gray; //保存转换后的灰度图像

	while(1)
	{
		cam >> image;
		cvtColor(image, gray, COLOR_BGR2GRAY);  //将3通道的彩色图像转换为单通道灰度图像
		equalizeHist(gray, gray);  //对图像进行均衡化处理，提高图像对比度，优化分类器的效果
		vector<Rect> faces;  //定义数组，保存检测到的人脸区域
		classifier.detectMultiScale(gray, faces); //检测人脸
		if (faces.size())  //是否找到人脸
		{
			rectangle(image, faces[0], CV_RGB(255, 0, 0)); //将找到的人脸区域用矩形框标记出来
		}
		imshow("Camera", image);
		if (waitKey(40) != 255)
		{
			break;
		}
	}
}

```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui -lopencv_imgproc -lopencv_objdetect
```

## REST API

curl是一个命令行下的HTTP客户端，可以通过命令的方式调用REST API。

```bash
curl -X GET http://coronavirus-tracker-api.herokuapp.com/v2/locations/225
```



通过C代码调用REST API

1. 安装libcurl函数库

```bash
sudo apt install libcurl4-gnutls-dev
```

2. 开发HTTP客户端程序

```c++
#include <curl/curl.h> //包含libcurl库的头文件

int main()
{
	CURL* client = curl_easy_init();  //初始化libcurl库，创建客户端句柄，类似于fopen

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");  //设置要访问的REST API地址
	curl_easy_perform(client); //默认执行GET方法，并将服务器返回的数据打印到标准输出

	curl_easy_cleanup(client);  //释放客户端句柄，类似于fclose
}

```

3. libcurl的API文档：https://curl.haxx.se/libcurl/c/
4. 编译命令：

```bash
g++ virus.cpp -lcurl
```

当没有设置CURLOPT_WRITEFUNCTION和CURLOPT_WRITEDATA选项的时候，libcurl使用fwrite和stdout作为默认值，因此HTTP服务器返回的数据会打印到标准输出上。

```c++
#include <curl/curl.h> //包含libcurl库的头文件

int main()
{
	CURL* client = curl_easy_init();  //初始化libcurl库，创建客户端句柄，类似于fopen

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");  //设置要访问的REST API地址
    //curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, fwrite);
    //curl_easy_setopt(client, CURLOPT_WRITEDATA, stdout);
	curl_easy_perform(client); //默认执行GET方法，并将服务器返回的数据打印到标准输出

	curl_easy_cleanup(client);  //释放客户端句柄，类似于fclose
}
```

将HTTP服务器返回的数据保存到字符串变量中

```c++
#include <curl/curl.h>
#include <string>
#include <iostream>

using namespace std;

//libcurl在处理服务器返回的数据时会多次调用回调函数将数据写入string对象
//ptr指针指向的内存保存服务返回的数据内容
//userdata就是指向string对象的指针
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
    //将最后一个参数强制转换为string对象的指针
	string* jsonstr = static_cast<string*>(userdata);
	jsonstr->append(ptr, size*nmemb);
	return size*nmemb;
}

int main()
{
	CURL* client = curl_easy_init();
	string jsonstr;

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");
    //注册写入数据的回调函数
	curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, write_callback);
    //设置回调函数的最后一个参数
	curl_easy_setopt(client, CURLOPT_WRITEDATA, &jsonstr);
	curl_easy_perform(client);
	//打印服务器返回的数据
	cout << jsonstr << endl;

	curl_easy_cleanup(client);
}

```

使用JSONCPP解析服务器返回的数据

jsoncpp代码库：https://github.com/open-source-parsers/jsoncpp

安装：

```bash
sudo apt install libjsoncpp-dev
```

```c++
#include <curl/curl.h>
#include <string>
#include <iostream>
#include <jsoncpp/json/json.h>  //jsoncpp的头文件

using namespace std;
using namespace Json;  //引用名字空间

size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
	string* jsonstr = static_cast<string*>(userdata);
	jsonstr->append(ptr, size*nmemb);
	return size*nmemb;
}

int main()
{
	CURL* client = curl_easy_init();
	string jsonstr;

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");
	curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(client, CURLOPT_WRITEDATA, &jsonstr);
	curl_easy_perform(client);

	//cout << jsonstr << endl;

	Value json; //保存解析后的json对象
	Reader parser;
	parser.parse(jsonstr, json); //解析服务器返回的json字符串

    //将json对象转换为字符串
	//StyledWriter writer;
	//cout << writer.write(json) << endl;

    //获取json对象中的确诊和死亡人数
	cout << "confirmed: " << json["location"]["latest"]["confirmed"] << endl;
	cout << "deaths: " << json["location"]["latest"]["deaths"] << endl;

	curl_easy_cleanup(client);
}

```

编译命令：

```bash
g++ virus.cpp -lcurl -ljsoncpp
```

## 调用百度人脸识别API

下载百度人脸识别C++ SDK：https://ai.baidu.com/download?sdkId=82

解压后会创建一个aip-cpp-sdk目录，我们需要使用其中的头文件。

需要安装openssl的开发包

```bash
sudo apt install libssl-dev
```

在人脸检测代码的基础上进行修改：

```c++
#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include "aip-cpp-sdk/face.h"

using namespace cv;
using namespace std;

int main()
{
	string app_id = "人脸识别的APP ID";
	string api_key = "人脸识别的API Key";
	string secret_key = "人脸识别的secret key";
	aip::Face client(app_id, api_key, secret_key);//创建人脸识别客户端

	VideoCapture cam(0);
	CascadeClassifier classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");

	namedWindow("Camera");
	namedWindow("Face"); //显示发送给百度云进行识别的人脸区域

	Mat image;
	Mat gray;

	while(1)
	{
		cam >> image;
		cvtColor(image, gray, COLOR_BGR2GRAY);
		equalizeHist(gray, gray);
		vector<Rect> faces;
		classifier.detectMultiScale(gray, faces);
		if (faces.size())
		{
			rectangle(image, faces[0], CV_RGB(255, 0, 0));
			Mat face(image, faces[0]); //对原图像进行裁剪，仅保留人脸的矩形区域
			imshow("Face", face); //将人脸显示到小窗口
			vector<unsigned char> buf; //保存压缩后的人脸图片
			imencode(".jpg", face, buf); //将人脸图片压缩为jpeg格式
            //将jpeg格式的人脸图片编码为BASE64字符串
			string faceimg = aip::base64_encode((char*)buf.data(), buf.size());
            //将人脸图片发送给百度云进行识别，识别结果保存到json对象中
			Json::Value json = client.search(faceimg, "BASE64", "group1", aip::null);
			Json::StyledWriter writer;
            //将识别结果转换为json字符串，显示在标准输出上
			cout << writer.write(json) << endl;
		}
		imshow("Camera", image);
		if (waitKey(40) != 255)
		{
			break;
		}
	}
}

```

编译命令：

```bash
g++ face.cpp -I/usr/include/jsoncpp -lopencv_videoio -lopencv_core -lopencv_highgui -lopencv_objdetect -lopencv_imgproc -lopencv_imgcodecs -lcurl -ljsoncpp -lcrypto
```

